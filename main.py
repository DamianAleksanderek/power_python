
# zadania domowe (dzień 1)
# ################################################################
# Stwórz tablice zawierającą liczby podzielne przez 3 mniejsze niż 10000

lista = []
for x in range(1000):
    if x %3 == 0:
        lista.append(x)
    else:
        continue

print(lista)

######################################################################
 # Narysować choinkę z gwiazdek - dla zadanego n (tutaj n=3)

def print_weird_tree_2(n):
    tree = ['*']
    for i in range(1, n):
        line = '*' * (i + 1)
        tree.append(line)
        tree.insert(0, line)
    print('\n'.join(tree))

def print_weird_tree(n):
    for i in range(n, 0, -1):
        print("*" * i)
    for i in range(2, n+1):
        print("*" * i)

############################################################################
#Stwórz tablicę 100 liczb pierwszych;


lista =[] #tworzymy pusta liste

for x in range(2,1000):
    if len(lista) < 100: # ograniczamy ilośc elementów w liście do 100
        for i in range(2, x):
            if x % i == 0:
                break
        else:
            lista.append(x)
print(lista)
print(len(lista))

# Napisz funkcję liczącą zapotrzebowanie na kalorie według wzoru Harrisa-Bennedicta

x = int(input("Podaj swoją wagę: "))
y = int(input("Podaj swój wzrost: "))
z = int(input("Podaj swój wiek: "))
s = input("Podaj swoją płeć w formacie M lub K ")
s = " "

def func_kalc (x, y, z):
    k = (10 * x) + (6.25 * y) - (5 * z)
    if s == "M" or "m":
        return k +5
    elif s == "K" or "k":
        return k -161



if s == "K" or "k":
    print("Twoja podstawa kaloryczna wynosi:", func_kalc(x,y,z))

elif s == "M" or "m":
    print("Twoja podstawa kaloryczna wynosi:", func_kalc(x,y,z))

print("Określ stopień aktywności fizycznej i podaj współczynnik: ")
print("1.2  *Praca siedząca, brak dodatkowej aktywności fizycznej*")
print("1.4  *Praca niefizyczna, mało aktywny tryb życia*")
print("1.6  *Lekka praca fizyczna,  regularne ćwiczenia 3-4 razy (ok. 5h) w tygodniu*")
print("1.8  *Praca fizyczna, regularne ćwiczenia od 5razy (ok. 7h) w tygodniu*")
print("2.0  *Praca fizyczna ciężka, regularne ćwiczenia 7razy w tygodniu*")
param = float(input())

print("Dzienne zapotrzebowanie kaloryczne to:", float(func_kalc(x,y,z) * param))