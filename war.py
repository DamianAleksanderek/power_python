'''Card game implementation'''

from enum import Enum
import random

class Card(Enum):
    C2 = 2, "2"
    C3 = 3, "3"
    C4 = 4, "4"
    C5 = 5, "5"
    C6 = 6, "6"
    C7 = 7, "7"
    C8 = 8, "8"
    C9 = 9, "9"
    C10 = 10, "10"
    JACK = 11, "J"
    QUEEN = 12, "Q"
    KING = 13, "K"
    ACE = 14, "A"

    def __str__(self):
        return self.value[1]
    
    def __lt__(self, other):
        return self.value[0] < other.value[0]

def display(cards):
    for x in cards:
        print(str(x).ljust(3), end="")
    print("")


class Game:
    def __init__(self):
        self.player1 = []
        self.player2 = []
        self.round_number = 1
        self.deal()

    def deal(self):
        deck = []
        for i in range(4):
            deck.extend(list(Card))

        random.shuffle(deck)
        self.player1 = deck[:int(len(deck)/2)]
        self.player2 = deck[int(len(deck)/2):]

    def war(self, card1, card2):
        print("WAR!!!")
        try:
            box = [card1, card2]
            card1 = self.player1.pop()
            card2 = self.player2.pop()
            box.append(card1)
            box.append(card2)
            card1 = self.player1.pop()
            card2 = self.player2.pop()
            random.shuffle(box)
            if card1 > card2:
                box.append(card1)
                box.append(card2)
                self.player1 = box + self.player1
                print(f"Player 1 wins ({card1} > {card2})")
                return 1
            elif card1 < card2:
                box.append(card1)
                box.append(card2)
                self.player2 = box + self.player2
                print(f"Player 2 wins ({card2} > {card1})")
                return 2
            else:
                if self.war(card1, card2) == 1:
                    self.player1 = box + self.player1
                else:
                    self.player2 = box + self.player2
        except IndexError:
            return

    def round(self):
        print(f"Player 1:{len(self.player1)}")
        display(self.player1)
        print(f"Player 2:{len(self.player2)}")
        display(self.player2)
        card1 = self.player1.pop()
        card2 = self.player2.pop()

        print(f'Round {self.round_number}')
        print(card1, card2)
        if card1 > card2:
            lista = [card1, card2]
            random.shuffle(lista)
            self.player1 = lista + self.player1
            print(f"Player 1 wins ({card1} > {card2})")
        elif card1 < card2:
            lista = [card1, card2]
            random.shuffle(lista)
            self.player2 = lista + self.player2
            print(f"Player 2 wins ({card2} > {card1})")
        else:
            self.war(card1, card2)

        self.round_number += 1
        if len(self.player1) == 0:
            print("Player 2 won the game")
            return False

        if len(self.player2) == 0:
            print("Player 1 won the game")
            return False
        
        return True

game = Game()

while game.round():
    input()
