# Zaimplementuj grę w Quiz w stolice.
# Użytkownik dostaje nazwę losowego kraju i musi odgadnąć jego stolicę.
# Użytkonwnik ma trzy życia.
# Program zliacza punktu.
# Każda poprawna odpowiedź +10 punktów.
# Traci jedno życie.
# Gra trwa popóki użytkownika ma życia.
# Nazwy krajów i ich stolic pobierz z internetu.
#
# Check lista:
# - stwórz kod źródłowy zgodny z PEP8;
# - przynajmniej jedno użycie class;
# - przynajmniej jedno użycie set lub dict
# - użyj if __name__ == "__main__";
# - podziel kod źródłowy na przynajmniej dwa moduły;
# - przynajmiej raz użyj try except;
#
# Deadline:12.04.2022 godizna 13:00;
# Rozwiązanie wkomituj w swoje repozytorium.

from bs4 import BeautifulSoup
import urllib.request
import requests
import random
from Capitals_dict import Dict


def main():
    points = 0
    life = 3

    while life > 0:
        player_choice = input("Play(p) or Quit(q): ")
        game = Dict()
        if player_choice == 'p':
            print("Get ready, let's start the game!!!")
            country, capital = game.random_country()
            print("Country : ", country)
            player_answer = input("Capital: ")
            if str(player_answer) == capital:
                print("Correct")
                points +=10
                print(f"Your point:", points)
                print(f"Life:", life)
            else:
                print("The wrong answer :(")
                print("The correct answer is:", capital)
                life -=1
                print(f"Your point:", points)
                print(f"Life:", life)
        elif player_choice == 'q':
            break
        else:
            print("Wrong Choice")


if __name__ == "__main__":
    main()
