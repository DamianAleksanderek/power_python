import math

class Point:
    def __init__(self, a, b, c, d, e):
        self.move(a, b, c, d, e)
    def move(self, a, b, c, d, e):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.e = e

    def dis(self, z):
        return math.sqrt((self.a - z.a)**2 + (self.b - z.b)**2 + (self.c - z.c)**2 + (self.d - z.d)**2 + (self.e - z.e)**2)

    def from_center(self):
        return self.dis(Point(0,0,0,0,0))



p = Point(3, 5, 8, 0, 0)
p1 = Point(3, 4, 8, 0, 0)
print(p.dis(p1))
print(p.from_center())
print(p1.from_center())