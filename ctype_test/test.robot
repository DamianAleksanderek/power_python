*** Settings ***

Test Template     Magic numbers
Library           MagicLibrary.py

*** Test Cases ***    Numbers            Expected
Simple test           3 2 4 2 1 5      -3
                      5 3 4 2 3 1      -4

Profit                1 2 4 4 5         0
                      3 4 7 9 10        0

Varied                3 2 10 7 15 14    -3



*** Keywords ***
Magic numbers
    [Arguments]    ${numbers}    ${expected}
    Magic function    ${numbers}  ${expected}

