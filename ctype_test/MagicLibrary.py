from ctypes import *
import os

lib_path = os.path.join(os.getcwd(), "libsum.so")
lib = CDLL(lib_path)
lib.magic_function.argtypes = (POINTER(c_int), c_int)
lib.magic_function.restype = c_int

class MagicLibrary:
    def magic_function(self, numbers, expected):
        numbers = numbers.split()
        numbers = list(map(int, numbers))
        n = len(numbers)
        array_type = c_int * n
        result = lib.magic_function(array_type(*numbers), c_int(n))
        if int(result) != int(expected):
            raise AssertionError (f"{result} != {expected}")



