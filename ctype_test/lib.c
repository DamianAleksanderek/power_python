#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int magic_function(int *data, int n)
{
    int high = -1;
    int low = -1;
    int max_loss = 0;
    for (int i = 0; i < n; i++) {
        int v = data[i];
        if (v > high) {
            high = v;
            low = -1;
        }
        if (low == -1 || v < low) {
            low = v;
        }
        if (high - low > max_loss) {
            max_loss = high - low;
        }
    }

    return -max_loss;
}