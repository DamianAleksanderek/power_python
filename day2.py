class Animal:
   pass

class Zebra(Animal):
    def speak(self):
        print("I'm Zebra")


class Rat(Animal):
    def speak(self):
        print("I'm Ratatuj")

class Lion(Animal):
    def speak(self):
        print("I'm Lion: AAAARRRRR")


class Bear(Animal):
    def speak(self):
        print("I'm Bear: RRRRRRRRRRHHHHHH")

class Python(Animal):
    def speak(self):
        print("I'm Pyton: Syntax Error")

class Dog(Animal):
    def eat(self):
        print("I eat meat")
    def speak(self):
        print("I'm Pyton: Syntax Error")

class Bird(Animal):
    def eat(self):
        print("I eat seed")
    def speak(self):
        print("I'm Pyton: **************")

class DogBird(Bird, Dog):
    pass

arka = [Zebra(), Zebra(), Rat(), Rat(), Lion(), Lion(), Bear(), Bear(), Python(), Python()]

a = DogBird()
a.eat()

for i in arka:
    i.speak()