import time
from bs4 import BeautifulSoup
from requests import get
import datetime
import pandas as pd



URL = 'https://www.olx.pl/nieruchomosci/mieszkania/sprzedaz/wroclaw'

def data_url(URL):
    flat_location = []
    flat_title = []
    time.sleep(2)
    page = get(URL)
    time.sleep(2)
    bs = BeautifulSoup(page.content, 'html.parser')

    for offer in bs.find_all('div', class_='offer-wrapper'):
        footer = offer.find('td', class_='bottom-cell')
        location = footer.find('small', class_='breadcrumb x-normal').get_text().strip().split(",")[0]
        title = offer.find('strong').get_text().strip()

        flat_location.append(location)
        flat_title.append(title)
    flat_olx = list(zip(flat_location, flat_title))
    return flat_olx

def list_to_dict(lista):
    dictionar = {}
    counter = 1
    for i in lista:
        dictionar[counter] = i
        counter += 1
    return dictionar

def save_data(dict, file_name):
    flat_datas = pd.DataFrame.from_dict(dict, orient='index', columns = ["Lokalizacja", "Tytuł ogłoszenia"])
    flat_datas.to_csv(file_name)

start = time.time()
flat_data_url = data_url(URL)
flat_list_to_dict = list_to_dict(flat_data_url)
save_data(flat_list_to_dict, 'nowe_ogłoszenia.csv')
stop = time.time()

duration = stop - start

print(f"{int(duration)} second.")
