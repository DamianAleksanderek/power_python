from bs4 import BeautifulSoup
import urllib.request
import requests
import random
import sys

url = "https://geographyfieldwork.com/WorldCapitalCities"

try:
    response = urllib.request.urlopen(url)
    soup = BeautifulSoup(response, 'html.parser')

except IOError:
    print('Failed to open url.')
    sys.exit()

table = soup.find('table', attrs={'class': 'sortable'})
values = table.find_all('tr')

countries_and_capitals = {}

for i in values:
    data = i.find_all('td')
    if len(data) == 0:
        continue
    country = data[0].getText()
    capital = data[1].getText()
    countries_and_capitals[country] = capital


class Dict:
    def __init__(self):
        self.countries_and_capitals = dict(countries_and_capitals)

    def random_country(self):
        country = random.choice(list(self.countries_and_capitals.keys()))
        capital = self.countries_and_capitals[country]
        return (country, capital)

