import collections
import os
import requests
import html_to_json
import threading
from collections import Counter
import unittest
from unittest import TestCase


def if_is_noun(word):
    url = "https://www.merriam-webster.com/dictionary/" + word
    response = requests.get(url)
    text_from_web = response.text
    dict_json = html_to_json.convert(text_from_web)
    try:
        tofile = dict_json["html"][0]['body'][0]['div'][0]['div'][0]['div'][5]['div'][0]['div'][0]['div'][0]['div'][0]['span'][0]['a'][0]['_value']
    except KeyError:
        return False
    if tofile == 'noun':
        return True
    else:
        return False

def read_the_file():
    for book in os.listdir("C:\\Users\\kjanuszyk\\Desktop\\hunger_games"):
        if book.endswith('.txt'):
            with open(os.path.join("C:\\Users\\kjanuszyk\\Desktop\\hunger_games", book), errors='ignore') as f:
                book_content = [word.lower() for line in f for word in line.split()]
            return Counter(book_content)


def sort_the_dict(word_count):
    word_count1 = sorted(word_count.items(),reverse=True, key=lambda x: x[1])
    return word_count1

word_count = read_the_file()
def main():
    new_dict = {}
    for noun in sort_the_dict(word_count):
        word = noun[0]
        value = noun[1]
        if if_is_noun(word) == True:
            new_dict[word] = value
            print(new_dict)
        if len(new_dict) > 9:
            break
    print(new_dict)

def test_word_is_noun():
    result = if_is_noun('back')
    assert result == True

def test_sort_the_dict():
    result = sort_the_dict(word_count)
    assert result[0][1] > result[-1][1]

if __name__ == '__main__':
    test_word_is_noun()
    test_sort_the_dict()
    main()