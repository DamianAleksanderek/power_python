# Wykorzystanie __lt__ , __init__

#
# class Person:
#     def __init__(self, wiek, kasa):
#         self.wiek = wiek
#         self.kasa = kasa
#
#     def __int__(self):
#         return self.wiek
#
#     def __lt__(self, other):
#         return self.kasa < other.kasa
#
# a = Person(22, 300)
# b = Person(33, 500)
# print(int(a))
# print(a < b)
# *************************************************************
# Obsługa wyjątków
#
# class Niemiec:
#     def __init__(self, wiek, gender):
#         self.wiek = wiek
#         self.gender = gender
#
#     def __mul__(self, other):
#         if self.gender == other.gender:
#             raise Exception("Ta sama płeć, nie bedzie z tego dzieci")
#         if self.wiek < 13 and other.wiek <13:
#             raise ("Osoba jest za młoda")
#         if 13 < self.wiek < 16 and 13 < other.wiek <16:
#             return Niemiec(self.wiek, self.gender)
#         elif self.wiek >= 16 and other.wiek >= 16:
#             return Niemiec(self.wiek, self.gender)
#         else:
#             raise Exception("Osoba jest za młoda")
#
#     def __repr__(self):
#         return f"wiek: {self.wiek}, gender: {self.gender}"
#
# helmut = Niemiec(14, "M")
# helga = Niemiec(15, "M")
# maly = helmut * helga
# print(maly)


# ********************************************************
# a = {"bokobrody", "grzywa", "czubek"}
# b = {"wąsy", "broda", "czubek"}
# c = a & b
# d = a or b
# e = a ^ b
# print(c)
# print(d)
# print(e)







