class Calculator(object):
    BUTTONS = '1234567890+-*/CDπ='

    def __init__(self):
        self._expression = ''

    def push(self, button):
        if button not in self.BUTTONS:
            raise CalculationError("Invalid button '%s'." % button)
        if button == '=':
            self._expression = self._calculate(self._expression)
        elif button == 'C':
            self._expression = ''
        elif button == '/':
            self._expression += '//'    # Integer division also in Python 3
        elif button == 'π':
            self._expression = self._calculate(str(int(self._expression) * 3))
        elif button == 'D':
            self._expression = self._calculate(self._expression[:-1])
        else:
            self._expression += button
        return self._expression

    def _calculate(self, expression):
        try:
            return str(eval(expression))
        except SyntaxError:
            raise CalculationError('Invalid expression.')
        except ZeroDivisionError:
            raise CalculationError('Division by zero.')

class CalculationError(Exception):
    pass
